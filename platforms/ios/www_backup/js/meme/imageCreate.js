  /*
            File Name   :      imageCreate.js
            Project     :      AEOM IOS App
            Copyright (c)      www.alleyezonmethemovie.com
            author      :      Prasanna 
            license     :   
            version     :      0.0.1 
            Created on  :      July ‎16, ‎2016
            Last modified on:  August ‎08, ‎2016 
            Description :      This file contains all meme functions.                          . 
            Organisation:      Peafowl inc.  
            */

  // Select Image
  function readURL(input) {
      if (input.files && input.files[0]) {
          var reader = new FileReader();

          reader.onload = function(e) {
              $('#image')
                  .attr('src', e.target.result);
              //.width(580)
              // .height(400);
              $('#modal').modal('show');
              $('#modal').modal({
                  backdrop: 'static',
                  keyboard: false
              });
          };
          reader.readAsDataURL(input.files[0]);
      }
     // getLatLan();
   }

  $(function() {
      var $image = $('#image');
      var cropBoxData;
      var canvasData;
      var $button = $('#button');
      var $result = $('#result');
      var croppedCanvas;
      var roundedCanvas;
      var croppable = false;
      var $dataRotate = $('#dataRotate');
      // Methods
      $('.docs-buttons').on('click', '[data-method]', function() {
          var $this = $(this);
          var data = $this.data();
          var $target;
          var result;

          if ($this.prop('disabled') || $this.hasClass('disabled')) {
              return;
          }
          if ($image.data('cropper') && data.method) {
              data = $.extend({}, data); // Clone a new one

              if (typeof data.target !== 'undefined') {
                  $target = $(data.target);

                  if (typeof data.option === 'undefined') {
                      try {
                          data.option = JSON.parse($target.val());
                      } catch (e) {
                          console.log(e.message);
                      }
                  }
              }

              result = $image.cropper(data.method, data.option, data.secondOption);

              switch (data.method) {
                  case 'scaleX':
                  case 'scaleY':
                      $(this).data('option', -data.option);
                      break;

                  case 'getCroppedCanvas':
                      if (result) {

                          // Bootstrap's Modal
                          $('#getCroppedCanvasModal').modal().find('.modal-body').html(result);

                          if (!$download.hasClass('disabled')) {
                              $download.attr('href', result.toDataURL('image/jpeg'));
                          }
                      }

                      break;
              }

              if ($.isPlainObject(result) && $target) {
                  try {
                      $target.val(JSON.stringify(result));
                  } catch (e) {
                      console.log(e.message);
                  }
              }

          }
      });

      $('#modal').on('shown.bs.modal', function() {
          $image.cropper({
              //aspectRatio: 1/1,
              minCropBoxWidth: 250,
              minCropBoxHeight: 250,
              autoCropArea: 1,
              viewMode: 2,
              built: function() {
                  croppable = true;
                  $image.cropper('setCanvasData', canvasData);
                  $image.cropper('setCropBoxData', cropBoxData);
              },
              crop: function(e) {
                  $dataRotate.val(e.rotate);
              }
          });
      }).on('hidden.bs.modal', function() {
          cropBoxData = $image.cropper('getCropBoxData');
          canvasData = $image.cropper('getCanvasData');
          $image.cropper('destroy');
      });

      $button.on('click', function() {
	
         // if ($("#hoverTxt").val()) 
		 // {
			  
              
            //  $(".memelogoTxt").html($("#hoverTxt").val());
              setTimeout(function() {
                //  $("#hoverTxt").css("background-color", "#333");
                  if (!croppable) {
                      return;
                  }
                  croppedCanvas = $image.cropper("getCroppedCanvas");
                  roundedCanvas = getRoundedCanvas(croppedCanvas);
                  $("#modal").modal("hide");
                  $(".memeLogo").hide();
                  $("#uimagediv").css("display", "none");


                  $("#saveimage").attr("src", roundedCanvas.toDataURL())
                      .width("100% !important;")
                      .height("auto");
              }, 50);
                
                  setTimeout(function(){ 

                 $("#cropLoader").show();
                 $("#memeimagediv").css("display", "block");
                  $(".divcentersave").css("display", "block");
                  $("#widget").show();


                  },200);

             



          
		  
		 // } else {
              //alert('Please enter Name'); 
          //    $("#hoverTxt").css("background-color", "#ff3333");
         // }
      });
  
    //getLatLan();
  });

  function getRoundedCanvas(sourceCanvas) {
      var canvas = document.createElement('canvas');
      var context = canvas.getContext('2d');
      var width = sourceCanvas.width;
      var height = sourceCanvas.height;
      canvas.width = width;
      canvas.height = height;
      context.beginPath();
      //  context.stroke();
      context.drawImage(sourceCanvas, 0, 0, width, height);
      return canvas;
  }

 // Save Image
  $(function() {
      $("#btnSave").click(function() {
          $('.divcentersave').css('display', 'none');
           html2canvas($("#widget"), {

              onrendered: function(canvas) {
                  theCanvas = canvas;
                  document.body.appendChild(canvas);
                  $('#memeimagediv').css('display', 'none');
                  $('#memesavediv').css('display', 'block');
                  $("#img-out").append(canvas);
                  canvas.id = 'smiley_canvas';
                  saveMeme();
              }
          });
      });
 // getLatLan();
  });

  $('[type="file"]').ezdz({
      text: 'Upload or Drag your Photo Inside the box',

      reject: function(file, errors) {
          if (errors.mimeType) {
              alert(file.name + ' must be an image.');
          }
      }
  });

// Download Image
  function downloadImage() {
      var canvas = document.getElementById("smiley_canvas");
      var image = canvas.toDataURL();

      var aLink = document.createElement('a');
      var evt = document.createEvent("HTMLEvents");
      evt.initEvent("click");
      aLink.download = 'image.png';
      aLink.href = image;
      aLink.dispatchEvent(evt);
  }

// Save Meme Image to Server
  function saveMeme() {  
     // getLatLan();
      $(".loadTxtHold").show();
      $("#meme_loading").html('Creating Image...');
      var canvas = document.getElementById("smiley_canvas");
      var image = canvas.toDataURL();
    //  var mname=$("#hoverTxt").val();
      var mzipcode='';
      var mlatitude=$("#lat").val();
      var mlongitude=$("#lan").val();



           var UserID = $("#user_Id").val();
                if(UserID) {

         $.ajax({
           url: serverURL + "/memeUpload",
           type: "POST",
          //data: '{"saveclick":{"imagedata":"' + image + '"}}',
          data: '{"memeUpload":{"imagedata":"'+ image +'","dpname":"","zipcode":"'+ mzipcode +'","latitude":"'+ mlatitude +'","longitude":"'+ mlongitude +'","userId":"'+ UserID+'"}}',

          contentType: "application/json; charset=utf-8",
          dataType: "json",
          success: function(data) {
            console.log("Meme Success:"+data);
              $("#meme_loading").html('Success');
             
              setTimeout(function() {
                  $("#meme_loading").html("");
                  $(".loadTxtHold").hide();
              }, 3000);


              $('#shareIcons').css('display', 'block');
              $("#memeURL").val(serverURL + '/meme/' + data['imageurl']);
          },
          error: function(jqXHR, textStatus, errorThrown) {
              $("#meme_loading").html(errorThrown);
              setTimeout(function() {
                  $("#meme_loading").html("");
                  $(".loadTxtHold").hide();
                  $("#divcentersave").show();
              }, 3000);

          }
      });


                } else{
                    // show pairing screen
                }   




      /* var myDB = window.sqlitePlugin.openDatabase({ name: 'AeomDB.db', location: 'default' }, null, null);
    myDB.transaction(function(transaction) {
        transaction.executeSql('SELECT * FROM aeomSettings', [], function(tx, results) {
           // var len = results.rows.length,
            var len = 1,
                i = 0;
            if (len > 0) {
                
              //  var UserID = results.rows.item(i).key3;
                var UserID = $("#user_Id").val();

             




                   } else {
                console.log('No user found.');
                $('.preloader').hide();
                $( ".joinbtn" ).click(); 
            }
        }, null);
    });  */


  }

  var success = function(msg) {
      $("#meme_loading").html('Image Saved to Gallery');
      $(".loadTxtHold").show();
      setTimeout(function() {
      $("#meme_loading").html("");
      $(".loadTxtHold").hide();

      }, 3000);
  };

  var error = function(err) {
      $("#meme_loading").html('Error While Saving Image:' + err);
      $(".loadTxtHold").show();

      setTimeout(function() {
      $("#meme_loading").html('');
      $(".loadTxtHold").hide();
      }, 3000);
  };
// Save Meme Image to Local Storage
  function saveImageToPhone(url, success, error) {
      $("#meme_loading").html('Downloading...');
      $(".loadTxtHold").show();
      var canvas, context, imageDataUrl, imageData;
      var img = new Image();
      img.onload = function() {
          canvas = document.createElement('canvas');
          canvas.width = img.width;
          canvas.height = img.height;
          context = canvas.getContext('2d');
          context.drawImage(img, 0, 0);
          try {
              imageDataUrl = canvas.toDataURL('image/jpeg', 1.0);
              imageData = imageDataUrl.replace(/data:image\/jpeg;base64,/, '');
              cordova.exec(
                  success,
                  error,
                  'Canvas2ImagePlugin',
                  'saveImageDataToLibrary', [imageData]
              );
          } catch (e) {
              error(e.message);
          }
      };
      try {
          img.src = url;
      } catch (e) {
          error(e.message);
      }
  }

 
/*  function fblink() {

      $("span").click(function() {
          var canvas = document.getElementById("smiley_canvas");
          var image = canvas.toDataURL();
          var url = $(this).attr("title");
          window.open('https://www.facebook.com/sharer/sharer.php?u=' + image, 'Share Facebook', config = 'height=300, width=500');
      });

  }*/
// Share Meme Image to Facebook
  $(".meme_fb").click(function() {
      var url = $('#memeURL').val();  
      window.open('https://www.facebook.com/sharer/sharer.php?u=' + url, 'Share Facebook', 'height=300, width=500');
  });
// Share Meme Image to Twitter
  $(".meme_twitter").click(function() {
      var url = $('#memeURL').val();
      window.open('http://twitter.com/home?status=Currently inspired by ' + url, 'Share Twitter', 'height=300, width=500');
  });
// Share Meme Image to Google Plus
  $(".meme_googlep").click(function() {
      var url = $('#memeURL').val();
      window.open('https://plus.google.com/share?url=' + url, 'Share Google +', 'height=300, width=500');
  });
// Share Meme Image to Pinterest
  $(".meme_pinterest").click(function() {
      var url = $('#memeURL').val();
      window.open('http://www.pinterest.com/pin/create/button/?url=' + url + '&media=' + url + '&description=Currently%20Inspired%20By', 'Share Pinterest', 'height=300, width=500');

  });
